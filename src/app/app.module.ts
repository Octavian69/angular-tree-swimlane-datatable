//Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BootstrapModule } from './modules/bootstrap.module';
import { TreeModule } from 'angular-tree-component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

// Services


//Components
import { AppComponent } from './app.component';
import { AngularTreeComponent } from './components/tree/tree.component';
import { DatatableComponent } from './components/datatable/datatable.component';

@NgModule({
  declarations: [
    AppComponent,
    AngularTreeComponent,
    DatatableComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BootstrapModule,
    TreeModule.forRoot(),
    NgxDatatableModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
