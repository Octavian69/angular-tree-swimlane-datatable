import { Component, OnInit } from '@angular/core';
import { DatatableService, Column, Row } from 'src/app/services/datatable.service';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})
export class DatatableComponent implements OnInit {

  isLoad: boolean = true;
  expanded: boolean = true;

  columns: Column[];
  rows: Row[];
  selected: any[] = [];

  constructor(
    private tableService: DatatableService
  ) { }

  ngOnInit() {
      this.rows = this.tableService.getInfo('rows');
      this.columns = this.tableService.getInfo('columns');
  }


  onPage(event) {
    console.log(event);
  }

  getRowClass(row) {
    return {
      'male': row.gender === 'Male',
      'female': row.gender === 'Female'
    }
  }

  activate(event) {
    console.log(event)
  }

  detailToggle(event) {
    console.log(event)
  }

  reorder(event) {
    console.log(event)
  }

  tableContextmenu(ev) {
    const { event } = ev;
    event.preventDefault();
    console.log(ev)
  }

  select(event) {
    this.selected = [...event.selected];
    console.log(this.selected)
  }

  sort(event) {
    console.log(event)
  }

  onDetailToggle(event) {
    console.log(event)
  }

  remove() {
    const selectIds = this.selected.map(item => item.id);
    this.rows = this.rows.filter(item => !selectIds.includes(item.id));
    this.selected = [];
  }

  reset() {
    this.selected = [];
  }

}
