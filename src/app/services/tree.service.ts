import { TreeNode } from "angular-tree-component";
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TreeService {

    activeNode: TreeNode;

    public getActiveNode(): TreeNode {
        return this.activeNode;
    }

    public setActiveNode(node: TreeNode | null): void {
        this.activeNode = node
    }
}