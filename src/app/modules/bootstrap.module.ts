import { NgModule } from '@angular/core';
import { TabsModule } from 'ngx-bootstrap/tabs';

const bsModules = [
  TabsModule.forRoot()
]


@NgModule({
  imports: bsModules,
  exports: [TabsModule]
})
export class BootstrapModule { }
