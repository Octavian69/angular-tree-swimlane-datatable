import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export type Row = {
  id: number,
  name: string,
  gender: string,
  company: string
};

export type Column = {
  name?: string,
  prop?: string,
  sortable?: boolean,
  checkboxable?: boolean,
  headerCheckboxable?: boolean,
};

@Injectable({
  providedIn: 'root'
})
export class DatatableService {

  columns: Column[] = [
    {prop: 'name'},
    {prop: 'gender', headerCheckboxable: true, checkboxable: true},
    {prop: 'company', sortable: false}
  ]

  rows: Row[] = [
    {
      id: 1,
      name: 'Henry',
      gender: 'Male',
      company: 'Burger King'
    },
    {
      id: 2,
      name: 'Harold',
      gender: 'Male',
      company: 'Mc Donald\'s'
    },
    {
      id: 3,
      name: 'Harry',
      gender: 'Male',
      company: 'KFC'
    },
    {
      id: 4,
      name: 'Hovard',
      gender: 'Male',
      company: 'Chiken House'
    },
    {
      id: 5,
      name: 'Helen',
      gender: 'Female',
      company: 'Subway'
    },
    {
      id: 6,
      name: 'Niel',
      gender: 'Male',
      company: 'Google'
    },
    {
      id: 7,
      name: 'John',
      gender: 'Male',
      company: 'Facebook'
    },
    {
      id: 8,
      name: 'Larry',
      gender: 'Male',
      company: 'VK'
    },
    {
      id: 9,
      name: 'Jack',
      gender: 'Male',
      company: 'Telegram'
    },
    {
      id: 10,
      name: 'Lora',
      gender: 'Female',
      company: 'Tor Browser'
    },
    {
      id: 1,
      name: 'Henry',
      gender: 'Male',
      company: 'Burger King'
    },
    {
      id: 2,
      name: 'Harold',
      gender: 'Male',
      company: 'Mc Donald\'s'
    },
    {
      id: 3,
      name: 'Harry',
      gender: 'Male',
      company: 'KFC'
    },
    {
      id: 4,
      name: 'Hovard',
      gender: 'Male',
      company: 'Chiken House'
    },
    {
      id: 5,
      name: 'Helen',
      gender: 'Female',
      company: 'Subway'
    },
    {
      id: 6,
      name: 'Niel',
      gender: 'Male',
      company: 'Google'
    },
    {
      id: 7,
      name: 'John',
      gender: 'Male',
      company: 'Facebook'
    },
    {
      id: 8,
      name: 'Larry',
      gender: 'Male',
      company: 'VK'
    },
    {
      id: 9,
      name: 'Jack',
      gender: 'Male',
      company: 'Telegram'
    },
    {
      id: 10,
      name: 'Lora',
      gender: 'Female',
      company: 'Tor Browser'
    },
  ]

  companyRows = [
    
      {
          "id": 0,
          "name": "Ramsey Cummings",
          "gender": "male",
          "age": 52,
          "address": {
              "state": "South Carolina",
              "city": "Glendale"
          }
      },
      {
          "id": 1,
          "name": "Stefanie Huff",
          "gender": "female",
          "age": 70,
          "address": {
              "state": "Arizona",
              "city": "Beaverdale"
          }
      },
      {
          "id": 2,
          "name": "Mabel David",
          "gender": "female",
          "age": 52,
          "address": {
              "state": "New Mexico",
              "city": "Grazierville"
          }
      },
      {
          "id": 3,
          "name": "Frank Bradford",
          "gender": "male",
          "age": 61,
          "address": {
              "state": "Wisconsin",
              "city": "Saranap"
          }
      },
  ]

  constructor(
    private http: HttpClient
  ) {}
  

  public getInfo<T>(key): T {
    return this[key];
  }
}
