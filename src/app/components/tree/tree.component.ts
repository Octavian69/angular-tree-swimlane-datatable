import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { ITreeOptions, TreeComponent, KEYS, TREE_ACTIONS, IActionMapping, ITreeState, TreeNode } from 'angular-tree-component';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';

import { TreeService } from 'src/app/services/tree.service';
import { StorageService } from 'src/app/services/storage.service';


@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class AngularTreeComponent implements OnInit, AfterViewInit {

  @ViewChild('tree') tree: TreeComponent;
  @ViewChild('filter') filter: ElementRef;

  nodes = [];
  newNode: string = '';
  activeNodes;
  activeNode: TreeNode;
  state: ITreeState;

  nodesTree = [
    {
      id: 1,
      name: 'root-1',
      children: [
        {
          id: 1.1,
          name: 'root-1.1',
          children: [
            {
              id: 1.11,
              name: 'root-1.1.1'
            },
            {
              id: 1.12,
              name: 'root-1.1.2'
            },
            {
              id: 1.13,
              name: 'root-1.1.3'
            },
          ]
        },
        {
          id: 1.2,
          name: 'root-1.2'
        }
      ]
    },
    {
      id: 2,
      name: 'root-2',
      children: [
        {
          id: 2.1,
          name: 'root-2.1'
        }
      ]
    },
    {
      id: 3,
      name: 'root-3'
    }
  ]

  actionMapping: IActionMapping = {
    mouse: {
      dblClick: (tree, node, event) => {
        TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, event);
      },
      contextMenu: (tree, node, event) => {
        event.preventDefault();
        TREE_ACTIONS.COLLAPSE(tree, node, event);
      }
    },
    keys: {
      [KEYS.ENTER]: (tree, node, event) => {
        TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, event)
      },
      [KEYS.RIGHT]: (tree, node, event) => {
        TREE_ACTIONS.DRILL_DOWN(tree, node, event);
      }
    }
  }
  counter: number = 0;

  asyncDate = [{
    id: 2.11,
    name: 'root-2.1.1',
    fieldName: 'stroke-2.1.1',
    hasChildren: true
  }]

  options: ITreeOptions = {
    allowDrag: (node) => true,
    allowDrop: (element, { parent, index }) => {
      return parent.hasChildren;
    },
    displayField: 'fieldName',
    animateSpeed: 10,
    levelPadding: 25,
    animateExpand: true,
    scrollOnActivate: true,
    animateAcceleration: 1.2,
    actionMapping: this.actionMapping,
    getChildren: (node: TreeNode) => {
        // if(!node.children) node.data.hasChildren = false;
        // return [];
        return of(this.asyncDate).pipe(
          map((response: any) => {
            response = response.map(item => {
              item.hasChildren = item.children ? true : false;
              return item;
            });
          return response;
        })
      ).toPromise()
    }
  }

  constructor(
    private storage: StorageService,
    private treeService: TreeService
  ) { }

//   TreeModel.prototype.getNodeById = function (id) {
//     var idStr = id.toString();
//     return this.getNodeBy(function (node) { return node.id.toString() === idStr; });
// };
// TreeModel.prototype.getNodeBy = function (predicate, startNode) {
//     if (startNode === void 0) { startNode = null; }
//     startNode = startNode || this.virtualRoot;
//     if (!startNode.children)
//         return null;
//     var found = find(startNode.children, predicate);
//     if (found) { // found in children
//         return found;
//     }
//     else { // look in children's children
//         for (var _i = 0, _a = startNode.children; _i < _a.length; _i++) {
//             var child = _a[_i];
//             var foundInChildren = this.getNodeBy(predicate, child);
//             if (foundInChildren)
//                 return foundInChildren;
//         }
//     }
// };

  ngOnInit() {
    if(!this.storage.getInfo('nodes')) {
      this.storage.setInfo('nodes', this.nodesTree);
    }

    const nodes: any[] = this.storage.getInfo('nodes');

    this.addNameField( nodes );

    this.nodes = nodes;

    const active = this.findNode(nodes, 1.13);
  }

  ngAfterViewInit() {
    // const active = this.tree.treeModel.getNodeBy((node) => {
    //   return node.data.fieldName === 'stroke-1.1.3';
    // });

    this.activeNode = this.treeService.getActiveNode() || null;

    if(this.activeNode) this.activeNode.setActiveAndVisible();
}

  findChildren = (nodes, id) => {
    let active = this.findNode(nodes, id);

    if(active.children && active.children.length) return active.children;
    else false;
  }


  findNode(nodes, id) {

     let active = null;

     function search(nodesArr) {

      for(let i = 0; i < nodesArr.length; i++) {

        if(nodesArr[i].id === id) {
          active = nodesArr[i];
          break; 
        } 

        if(nodesArr[i].children && nodesArr[i].children.length) search(nodesArr[i].children);
      }
    }

    search(nodes);

    return active;
  }

  addNameField(nodes, nameIteration?: string) {
    const name = nameIteration || 'stroke-'; 

    nodes.forEach((node, i) => {
      let idx = i + 1;
      node.fieldName = name + idx;
      node.hasChildren = true;
      if(node.children && node.children.length) this.addNameField(node.children, name + idx + '.');
    });
  }

  filterNodes(value: string): void {
    if(!value) {
      this.tree.treeModel.collapseAll();
      return;
    } 
    this.tree.treeModel.filterNodes(value);
  }

  clearFilter() {
    this.tree.treeModel.clearFilter();
    this.filter.nativeElement.value = '';
  }

  active(active) {
    this.treeService.setActiveNode(active.node);
  }


  deactivate(e) {
    this.treeService.setActiveNode(null);
  }

  selected(e) {

  }

  toggleTree(action: string): void {
    if(action === 'collapse') {
      this.tree.treeModel.collapseAll();
      console.log(this.state)
    } else {
      this.tree.treeModel.expandAll();
    }
  }

  public addNode(): void {
    const node: { fieldName: string, id: number } = {
      id: 4,
      fieldName: this.newNode
    };

    this.nodes.push( node );
    this.tree.treeModel.update();
    this.newNode = '';
  }

  public removeNode(): void {
    const remove = this.treeService.getActiveNode();
    
    if(remove) {

      if(remove.isRoot) {
        this.nodes = this.nodes.filter(node => node.id !== remove.data.id);
      } else {
        // _.remove(remove.parent.data.children, remove.data);
        remove.parent.data.children = remove.parent.data.children.filter(item => item.id !== remove.data.id);
      }
 
    }

    this.tree.treeModel.update();
  }
}
