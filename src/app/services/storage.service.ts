import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    getInfo<T>(key: string): T {
        const info = window.sessionStorage.getItem(key);
        return JSON.parse(info);
    }

    setInfo<T>(key: string, data: T): void {
        const info = JSON.stringify(data);
        window.sessionStorage.setItem(key, info);
    }
} 